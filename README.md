# WaterOverflowProblem

## Running the application

ConsoleApplication.jar can be run from the command line using the latest version of Java (e.g., `"C:\Program Files\Java\jdk-13.0.2\bin\java.exe" -jar ConsoleApplication.jar`).

## Notes

This application demonstrates an object-oriented approach, and is not optimised. As a result, pouring much more than ~30 litres into the top glass may result in very long run times.

If optimisation were required, I would recommend abandoning the object-oriented approach and using a simple equation to calculate liquid volumes.