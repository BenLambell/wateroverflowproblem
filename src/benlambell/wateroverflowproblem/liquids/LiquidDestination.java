package benlambell.wateroverflowproblem.liquids;

public interface LiquidDestination {
	public void addLiquidVolume(double addedLiquidVolume);
}
