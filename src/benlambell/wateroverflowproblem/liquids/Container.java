package benlambell.wateroverflowproblem.liquids;

public class Container implements LiquidDestination {
	private final double volume;
	private double liquidVolume;
	private LiquidDestination overflowDestination;
	
	public Container(double volume) {
		if (Double.isNaN(volume) || volume < 0) {
			throw new IllegalArgumentException("Volume must be a non-negative number.");
		}
		
		this.volume = volume;
	}
	
	public double getVolume() {
		return volume;
	}
	
	public double getLiquidVolume() {
		return liquidVolume;
	}
	
	public void addLiquidVolume(double addedLiquidVolume) {
		if (Double.isNaN(addedLiquidVolume) || addedLiquidVolume < 0) {
			throw new IllegalArgumentException("Volume must be a non-negative number.");
		}
		
		double overflowVolume = liquidVolume + addedLiquidVolume - volume;
		if (overflowVolume > 0) {
			liquidVolume = volume;
			if (overflowDestination != null) {
				overflowDestination.addLiquidVolume(overflowVolume);
			}
		} else {
			liquidVolume += addedLiquidVolume;
		}
	}
	
	public LiquidDestination getOverflowDestination() {
		return overflowDestination;
	}
	
	public void setOverflowDestination(LiquidDestination overflowDestination) {
		if (this == overflowDestination) {
			throw new IllegalArgumentException("Container cannot overflow into itself.");
		}
		
		this.overflowDestination = overflowDestination;
	}
}
