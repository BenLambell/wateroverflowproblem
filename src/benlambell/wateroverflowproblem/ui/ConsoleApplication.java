package benlambell.wateroverflowproblem.ui;

import java.util.InputMismatchException;
import java.util.Scanner;

import benlambell.wateroverflowproblem.liquids.Container;
import benlambell.wateroverflowproblem.model.LazyGlassPyramid;

public class ConsoleApplication {
	private static final double GLASS_VOLUME = 0.25;
	
	public static void main(String[] args) {
		var scanner = new Scanner(System.in);
		try {
			run(scanner);
			
			System.out.println("Press ENTER to quit.");
			scanner.nextLine();
		} finally {
			scanner.close();
		}
	}
	
	private static void run(Scanner scanner) {
		System.out.println("Water Overflow Problem Solver");
		
		double pouredVolume = 0;
		int rowIndex = 0;
		var columnIndex = 0;
		try {
			System.out.print("Enter litres poured into top glass: ");
			pouredVolume = scanner.nextDouble();
			System.out.print("Enter row index: ");
			rowIndex = scanner.nextInt();
			System.out.print("Enter column index: ");
			columnIndex = scanner.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Invalid input: " + e.getMessage());
			return;
		}
		
		Container glass = null;
		try {
			var pyramid = new LazyGlassPyramid(GLASS_VOLUME);
			pyramid.getGlass(0, 0).addLiquidVolume(pouredVolume);
			glass = pyramid.getGlass(rowIndex, columnIndex);
		} catch (Exception e) {
			System.out.println("An error occurred: " + e.getMessage());
			return;
		}

		System.out.println("Contents of glass: " + glass.getLiquidVolume() + "L");
	}
}
