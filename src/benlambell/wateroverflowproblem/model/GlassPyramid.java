package benlambell.wateroverflowproblem.model;

import benlambell.wateroverflowproblem.liquids.Container;

public interface GlassPyramid {
	public Container getGlass(int rowIndex, int columnIndex);
}
