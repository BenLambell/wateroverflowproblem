package benlambell.wateroverflowproblem.model;

import benlambell.wateroverflowproblem.liquids.Container;

public class LazyGlassPyramidGlass extends Container {
	public LazyGlassPyramidGlass(double volume) {
		super(volume);
	}

	@Override
	public void addLiquidVolume(double addedLiquidVolume) {
		if (Double.isInfinite(addedLiquidVolume)) {
			throw new IllegalArgumentException("Infinite liquid not supported.");
		}
		
		super.addLiquidVolume(addedLiquidVolume);
	}
}
