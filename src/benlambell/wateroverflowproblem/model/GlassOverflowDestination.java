package benlambell.wateroverflowproblem.model;

import benlambell.wateroverflowproblem.liquids.Container;
import benlambell.wateroverflowproblem.liquids.LiquidDestination;

public class GlassOverflowDestination implements LiquidDestination {
	private final GlassPyramid glassPyramid;
	private final int rowIndex;
	private final int columnIndex;
	
	public GlassOverflowDestination(GlassPyramid glassPyramid, int rowIndex, int columnIndex) {
		this.glassPyramid = glassPyramid;
		this.rowIndex = rowIndex;
		this.columnIndex = columnIndex;
	}
	
	@Override
	public void addLiquidVolume(double addedLiquidVolume) {
		Container left = glassPyramid.getGlass(rowIndex + 1, columnIndex);
		Container right = glassPyramid.getGlass(rowIndex + 1, columnIndex + 1);

		left.addLiquidVolume(addedLiquidVolume / 2);
		right.addLiquidVolume(addedLiquidVolume / 2);
	}
}
