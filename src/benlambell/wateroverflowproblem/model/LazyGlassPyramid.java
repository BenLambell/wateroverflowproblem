package benlambell.wateroverflowproblem.model;

import java.util.ArrayList;
import java.util.List;

import benlambell.wateroverflowproblem.liquids.Container;

public class LazyGlassPyramid implements GlassPyramid {
	private final double glassVolume;
	private List<List<Container>> rows = new ArrayList<List<Container>>();
	
	public LazyGlassPyramid(double glassVolume) {
		if (glassVolume <= 0) {
			throw new IllegalArgumentException("Glass volume must be positive.");
		}
		
		this.glassVolume = glassVolume;
	}
	
	@Override
	public Container getGlass(int rowIndex, int columnIndex) {
		if (rowIndex < 0) {
			throw new IllegalArgumentException("Row index cannot be negative.");
		} else if (columnIndex < 0) {
			throw new IllegalArgumentException("Column index cannot be negative.");
		} else if (columnIndex > rowIndex) {
			throw new IllegalArgumentException("Row does not contain enough columns.");
		}
		
		while (rows.size() <= rowIndex) {
			rows.add(new ArrayList<Container>());
		}
		
		List<Container> row = rows.get(rowIndex);
		while (row.size() <= columnIndex) {
			var overflowDestination = new GlassOverflowDestination(
				this, 
				rowIndex,
				columnIndex);
			var glass = new LazyGlassPyramidGlass(glassVolume);
			glass.setOverflowDestination(overflowDestination);
			
			row.add(glass);
		}

		return row.get(columnIndex);
	}
}
