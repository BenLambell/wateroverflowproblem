package benlambell.wateroverflowproblem.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import benlambell.wateroverflowproblem.model.LazyGlassPyramid;

public class ModelTest {
	private static final double EPSILON = 0;

	@Test
	public void assertLiquidIsPersistent() {
		var pyramid = new LazyGlassPyramid(1);

		pyramid.getGlass(4, 3).addLiquidVolume(.2);

		assertEquals(.2, pyramid.getGlass(4, 3).getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertOverflowDistributedToUnloadedGlasses() {
		var pyramid = new LazyGlassPyramid(1);

		pyramid.getGlass(4, 3).addLiquidVolume(2);

		assertEquals(.5, pyramid.getGlass(5, 3).getLiquidVolume(), EPSILON);
		assertEquals(.5, pyramid.getGlass(5, 4).getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertOverflowDistributedToPreloadedGlasses() {
		var pyramid = new LazyGlassPyramid(1);
		var left = pyramid.getGlass(5, 3);
		var right = pyramid.getGlass(5, 4);

		pyramid.getGlass(4, 3).addLiquidVolume(2);

		assertEquals(.5, left.getLiquidVolume(), EPSILON);
		assertEquals(.5, right.getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertOverflowDistributedToLoadedGlasses() {
		var pyramid = new LazyGlassPyramid(1);
		var top = pyramid.getGlass(4, 3);
		var left = pyramid.getGlass(5, 3);
		var right = pyramid.getGlass(5, 4);

		top.addLiquidVolume(2);

		assertEquals(.5, left.getLiquidVolume(), EPSILON);
		assertEquals(.5, right.getLiquidVolume(), EPSILON);
	}
}
