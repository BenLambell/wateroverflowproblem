package benlambell.wateroverflowproblem.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import benlambell.wateroverflowproblem.liquids.Container;

public class LiquidsTest {
	private static final double EPSILON = 0;
	
	@Test
	public void assertContainerInitiallyEmpty() {
		var container = new Container(1);

		assertEquals(0, container.getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertContainerCanPartiallyFill() {
		var container = new Container(1);
		
		container.addLiquidVolume(.5);

		assertEquals(.5, container.getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertContainerCanFill() {
		var container = new Container(1);
		
		container.addLiquidVolume(1);

		assertEquals(1, container.getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertContainerCanAddZeroLiquidVolume() {
		var container = new Container(1);
		
		container.addLiquidVolume(0);

		assertEquals(0, container.getLiquidVolume(), EPSILON);
	}

	@Test(expected = IllegalArgumentException.class)
	public void assertContainerCannotAddNegativeLiquidVolume() {
		var container = new Container(1);
		
		container.addLiquidVolume(-1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void assertContainerCannotAddNonNumberLiquidVolume() {
		var container = new Container(1);
		
		container.addLiquidVolume(Double.NaN);
	}

	@Test
	public void assertContainerOverflows() {
		var container = new Container(1);
		
		container.addLiquidVolume(2);

		assertEquals(1, container.getLiquidVolume(), EPSILON);
	}

	@Test
	public void assertContainerOverflowsIntoDestination() {
		var in = new Container(1);
		var out = new Container(1);
		in.setOverflowDestination(out);
		
		in.addLiquidVolume(1.5);

		assertEquals(1, in.getLiquidVolume(), EPSILON);
		assertEquals(.5, out.getLiquidVolume(), EPSILON);
	}
}
